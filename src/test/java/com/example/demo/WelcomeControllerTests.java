package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

// these static import makes it possible for you to write .andExpect(status().isOk()) instead of 
// .andExpect(MockMvcResultMatchers.status().isOk())
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;


@WebMvcTest(WelcomeController.class)
public class WelcomeControllerTests {
	
	@Autowired
	MockMvc mockMvc;
	
	@Test
	void sayHello_noParams_rtnHelloWorld() throws Exception {
		// "Hello World"
		// Arrange: setting up the test - prepare environment, setting up databases etc.
		// Act
		mockMvc.perform(get("/welcome"))
			// Assert - test results
			.andExpect(status().isOk())
			.andExpect(content().string("Hello World"));
		
	}
	
	@Test
	void sayHello_myName_rtnHelloName() throws Exception{
		mockMvc.perform(get("/welcome?name=Rob"))
		.andExpect(status().isOk())
		.andExpect(content().string("Hello Rob"));
	}
	
	
	
}
